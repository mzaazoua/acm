
# @file     acmSetup.sh
# @purpose  Sets up workspace for use with cmake and git.
# @author   Vincent Pascuzzi <vincent.pascuzzi@cern.ch, vpascuzz@physics.utoronto.ca>
# @date     March 2017

# Get the directory of acm root
myDir="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )" && pwd )"
myScripts=${myDir}/scripts

# Export things we need
export ACMDIR=${myDir}
export PYTHONPATH=${myDir}/python:$PYTHONPATH

# Temporary file written to by acmSetup.py
temp_setupfile=/tmp/acmsetup-$$
# Write configuration to file
python ${myScripts}/acmSetup.py -o $temp_setupfile "$@"
ret=$?

# Check if setup file was written
if [ -f ${temp_setupfile} ]
then
  source ${temp_setupfile}
  ret=$?
  rm -rf ${temp_setupfile}
fi

# Return nicely
return $ret
