# @file     acmsave.py
# @purpose  Provides functionality to read and write .acmsave file for a test area.
# @author   Vincent Pascuzzi <vincent.pascuzzi@cern.ch, vpascuzz@physics.utoronto.ca>
# @date     April 2017


class
