# @file     cmake.py
# @purpose  Provides functionality to read and modify `CMakeLists.txt` files.
# @author   Vincent Pascuzzi <vincent.pascuzzi@cern.ch, vpascuzz@physics.utoronto.ca>
# @date     May 2017


class CMakeUtils(object) :
  """description: this class provides functionality for reading `CMakeLists.txt`
  files. Every `CMakeLists.txt` is organised by keys which are used by this
  class to read in the information, e.g.
    atlas_subdir: contains the package name
    atlas_depends_on_subdirs: package dependencies, PUBLIC and PRIATE
    find_package: external dependencies
    atlas_add_library: libraries in the package
    atlas_install_headers: install files from the package
    atlas_install_joboptions: JO directory, usually `share`
  """

  def __init__(self, path):
    self.m_cmakelistsFile = path+'/CMakeLists.txt'
    self.m_cmakelistsContent = []   # holds CMakeLists content line-by-line
    self.m_packageName = ""
    self.m_dependsOnSubdir = []
    self.m_dependsOnSubdirPublic = []
    self.m_dependsOnSubdirPrivate = []
    self.m_externals = []
    self.m_libs = []
    self.m_libsSrc = []
    self.m_libsPublic = []
    self.m_libsPrivate = []
    self.m_executables = []
    self.m_comps = []
    self.m_compsLinkLibs = []
    self.m_dicts = []
    self.m_dictsLinkLibs = []
    self.m_pythonModules = []
    self.m_joboptions = []

  def read_cmakelists(self) :
    """ Reads the entire CMakeLists.txt file. This should be done first.
    """
    with open(self.m_cmakelistsFile) as f:
      self.m_cmakelistsContent = f.readlines()
    #remove whitespace and '\n'
    self.m_cmakelistsContent = [x.strip() for x in self.m_cmakelistsContent]
    pass

  def print_cmakelists(self) :
    print self.m_cmakelistsContent
    pass

  def printpretty_cmakelists(self) :
    print ('\n'.join(self.m_cmakelistsContent))
    pass

  def get_package_name(self) :
    """Get the package name from the `atlas_subdir` declaration.
    """
    atlas_subdir = next(s for s in self.m_cmakelistsContent if "atlas_subdir" in s)
    return atlas_subdir[atlas_subdir.find("(")+1:atlas_subdir.find(")")].strip()

  def get_atlas_depends_on_subdir(self) :
    """ Reads contents of `atlas_depends_on_subdirs`."""
    pass

  def get_atlas_depends_on_subdirs_public(self) :
    """Get only the `PUBLIC` dependencies.
    """
    pass

  def get_atlas_depends_on_subdirs_private(self) :
    """Get only the `PRIVATE` dependencies.
    """
    pass

  def get_externals(self) :
    """These are delcared in `find_package` declaration(s). There can be >1 of
    these.
    """
    pass

  def get_atlas_add_library(self) :
    pass

  def get_atlas_add_library_public(self) :
    pass

  def get_atlas_add_library_private(self) :
    pass

  def get_add_executables(self) :
    pass

  def get_add_component(self) :
    pass

  def get_add_components_linklibs(self) :
    pass

  def get_add_dictionary(self) :
    pass

  def get_add_dictionary_linklibs(self) :
    pass

  def get_install_python_modules(self) :
    pass

  def get_install_joboptions(self) :
    pass

  def write_cmakelists(self) :
    pass

  def write_atlas_subdir(self) :
    pass

  def write_package_name(self) :
    pass

  def write_depends(self) :
    pass

  def write_depends_on_subdirs(self) :
    pass

  def write_depends_on_subdirs_public(self) :
    pass

  def write_depends_on_subdirs_private(self) :
    pass

  def write_externals(self) :
    pass

  def write_add_library(self) :
    pass

  def write_add_library_public(self) :
    pass

  def write_add_library_private(self) :
    pass

  def write_executables(self) :
    pass

  def write_add_component(self) :
    pass

  def write_add_components_linklibs(self) :
    pass

  def write_add_dictionary(self) :
    pass

  def write_add_dictionary_linklibs(self) :
    pass

  def write_install_python_modules(self) :
    pass

  def write_install_joboptions(self) :
    pass
