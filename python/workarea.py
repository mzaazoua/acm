#!/usr/bin/env python

# @file     workarea.py
# @purpose  Provides functionality related to configuring and modifying an acm workarea.
# @author   Vincent Pascuzzi <vincent.pascuzzi@cern.ch, vpascuzz@physics.utoronto.ca>
# @date     May 2017

class WorkArea(object) :
  def __init__(self, acmbin=None, acmdir=None) :
    pass

  ## Creates a string of the environment variables to unset.
  def get_unsets(self) :
    unstr = ""
    # Format the unset string
    unstr += "unset ACMDIR\n"
    unstr += "unset ACMBIN\n"
    unstr += "unset ACMSOURCEDIR\n"
    unstr += "unset ACMBUILDDIR\n"
    unstr += remove_from_path("PATH",       [ACMBIN])
    unstr += remove_from_path("PYTHONPATH", [ACMPYTHON,ACMSCRIPTS])
    unstr += "unset ACM_GITLAB_PATH_PREFIX\n"
    # Return unset string
    return unstr
